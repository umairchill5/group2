package library.entities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Member implements Serializable {

	private String LaSt_NaMe;
	private String FiRsT_NaMe;
	private String EmAiL_AdDrEsS;
	private int PhOnE_NuMbEr;
	private int MeMbEr_Id;
	private double FiNeS_OwInG;
	
	private Map<Integer, Loan> cUrReNt_lOaNs;

	
	public Member(String lAsT_nAmE, String fIrSt_nAmE, String eMaIl_aDdReSs, int pHoNe_nUmBeR, int mEmBeR_iD) {
		this.LaSt_NaMe = lAsT_nAmE;
		this.FiRsT_NaMe = fIrSt_nAmE;
		this.EmAiL_AdDrEsS = eMaIl_aDdReSs;
		this.PhOnE_NuMbEr = pHoNe_nUmBeR;
		this.MeMbEr_Id = mEmBeR_iD;
		
		this.cUrReNt_lOaNs = new HashMap<>();
	}

	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Member:  ").append(MeMbEr_Id).append("\n")
		  .append("  Name:  ").append(LaSt_NaMe).append(", ").append(FiRsT_NaMe).append("\n")
		  .append("  Email: ").append(EmAiL_AdDrEsS).append("\n")
		  .append("  Phone: ").append(PhOnE_NuMbEr)
		  .append("\n")
		  .append(String.format("  Fines Owed :  $%.2f", FiNeS_OwInG))
		  .append("\n");
		
		for (Loan LoAn : cUrReNt_lOaNs.values()) {
			sb.append(LoAn).append("\n");
		}		  
		return sb.toString();
	}

	
	public int getId() {
		return MeMbEr_Id;
	}

	
	public List<Loan> getLoans() {
		return new ArrayList<Loan>(cUrReNt_lOaNs.values());
	}

	
	public int getNumberOfCurrentLoans() {
		return cUrReNt_lOaNs.size();
	}

	
	public double finesOwed() {
		return FiNeS_OwInG;
	}

	
	public void takeOutLoan(Loan lOaN) {
		if (!cUrReNt_lOaNs.containsKey(lOaN.getId())) 
			cUrReNt_lOaNs.put(lOaN.getId(), lOaN);
		
		else 
			throw new RuntimeException("Duplicate loan added to member");
				
	}

	
	public String GeT_LaSt_NaMe() {
		return LaSt_NaMe;
	}

	
	public String GeT_FiRsT_NaMe() {
		return FiRsT_NaMe;
	}


	public void addFine(double fine) {
		FiNeS_OwInG += fine;
	}
	
	public double PaY_FiNe(double AmOuNt) {
		if (AmOuNt < 0) 
			throw new RuntimeException("Member.payFine: amount must be positive");
		
		double change = 0;
		if (AmOuNt > FiNeS_OwInG) {
			change = AmOuNt - FiNeS_OwInG;
			FiNeS_OwInG = 0;
		}
		else 
			FiNeS_OwInG -= AmOuNt;
		
		return change;
	}


	public void dischargeLoan(Loan LoAn) {
		if (cUrReNt_lOaNs.containsKey(LoAn.getId())) 
			cUrReNt_lOaNs.remove(LoAn.getId());
		
		else 
			throw new RuntimeException("No such loan held by member");
				
	}

}
